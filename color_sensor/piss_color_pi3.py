import RPi.GPIO as GPIO
import time
import os
import random


s2 = 23
s3 = 24
signal = 25
NUM_CYCLES = 5

#categories
healthy=0
needs_improvement=0
dying_soon=0


def setup():
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(signal,GPIO.IN, pull_up_down=GPIO.PUD_UP)
  GPIO.setup(s2,GPIO.OUT)
  GPIO.setup(s3,GPIO.OUT)
  print("\n")
  




def loop():
  temp = 1
  time.sleep(3);
  while(1):  

    GPIO.output(s2,GPIO.LOW)
    GPIO.output(s3,GPIO.LOW)
    time.sleep(0.3)
    start = time.time()
    for impulse_count in range(NUM_CYCLES):
      GPIO.wait_for_edge(signal, GPIO.FALLING)
    duration = time.time() - start 
    red  = NUM_CYCLES / duration   
   
    GPIO.output(s2,GPIO.LOW)
    GPIO.output(s3,GPIO.HIGH)
    time.sleep(0.3)
    start = time.time()
    for impulse_count in range(NUM_CYCLES):
      GPIO.wait_for_edge(signal, GPIO.FALLING)
    duration = time.time() - start
    blue = NUM_CYCLES / duration
    

    GPIO.output(s2,GPIO.HIGH)
    GPIO.output(s3,GPIO.HIGH)
    time.sleep(0.3)
    start = time.time()
    for impulse_count in range(NUM_CYCLES):
      GPIO.wait_for_edge(signal, GPIO.FALLING)
    duration = time.time() - start
    green = NUM_CYCLES / duration
    
    if green<red and blue<red and temp==0 and 1==2:
      print("green: %f" % green)
      print("blue: %f" % blue)
      print("red: %f" % red)
      print("red")
      temp=1
    elif red<green and  blue<green and temp==0 and 1==2:
      print("green: %f" % green)
      print("blue: %f" % blue)
      print("red: %f" % red)
      print("green")
      temp=1
    elif green<blue and red<blue and temp==0 and 1==2:
      print("green: %f" % green)
      print("blue: %f" % blue)
      print("red: %f" % red)
      print("blue")
      temp=1
    elif red>10000 and green>10000 and blue>10000 and temp==1 and 1==2:
      print("place the object.....")
      temp=0
    
              
    healthy=green > 2500 and blue > 2500 and red > 5000
    needs_improvement=green>1000 and blue>1000 and red>3000
    dying_soon=0
    
    if(healthy):
        print("healthy %s" % healthy)
        healthyWords()
        dying_soon=1
        #time.sleep(3);
    elif(needs_improvement):
        print("needs_improvement %s" % needs_improvement)
        needsImprovementWords()
        dying_soon=1
        #time.sleep(3);
    else:
        print("dying_soon %s" % dying_soon)
        dyingSoonWords()
        dying_soon=1
        #time.sleep(3);

def endprogram():
    GPIO.cleanup()
    
def healthyWords():
    choices = ['good stuff, you are clean',
               'you gonna live till hundred',
               'are you sure you did not pour in a glass of water',
               'crystal clear urine! what a job',
               'refreshing! that really hists the spot',
               'you have urinated successfully, congratulations!']
    choices = random.choice(choices)
    os.system("espeak \"" + choices + "\" 2>/dev/null ")

def needsImprovementWords():
    choices = ['oh dear, what do we have here',
               'this urine has notes of caramel, mayonnaise, and himalayan rock salt...delightful',
               'you are in the borderline my friend'
               ]
    choices = random.choice(choices)
    os.system("espeak \"" + choices + "\" 2>/dev/null ")

def dyingSoonWords():
    choices = ['i think you are about to die, should I call an ambulance?',
               'how did you even make it to the toilet, you are basically already dead',
               'zombie pee, watch out'
               ]
    choices = random.choice(choices)
    os.system("espeak \"" + choices + "\" 2>/dev/null ")


if __name__=='__main__':
    
    setup()

    try:
        loop()

    except KeyboardInterrupt:
        endprogram()
